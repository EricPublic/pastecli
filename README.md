PasteCLI - PasteBin Command Line Interface
==========================================


Purpose
-------

[https://gitlab.com/EricPublic/pastecli](PasteCLI) is an utility to easily paste
bits of code (or other outputs) from the command line into so-called
[https://en.wikipedia.org/wiki/Pastebin](Pastebin) services, which makes
available to others, especially in chat tools, some longer piece of text via
a short link reference without flooding the chat room.


Installation
------------

There is currently no proper installation procedure but you can either:

1. `git clone` this repository or copy its content at any place, and call
   `./pastecli` in there.
2. copy the executable `pastecli` in your PATH (typically `/usr/local/bin` or
   `/usr/bin`) and the `services` directory to  `/usr/local/share/pastecli` or
   `/usr/share/pastecli`.


Synopsis
--------

A complete list of options are available using `pastecli -h` (or `--help`),
including a list of available Pastebin-services.

Pasting this current file could be as easy as writing `cat README.md | pastecli -s fedora -` or, with syntax and title directly from the file: `pastecli -s fedora -x python -t PasteCLI - README.md` (notice the dash **-** in front of the filename).

In return you'll get in both cases an URL that you can simply paste in your chat
for others' reference.


New services
------------

You can define new pastebin services by creating a fitting ini-file either
in `/etc/pastecli/services` or `~/.local/share/pastecli/services`.

TODO: the format isn't yet defined (nor completely stable) and needs some words
of explanation. In the meantime use the already existing ini-files as template.

Once you've done this, you're very welcome to provide it attached to an
[https://gitlab.com/EricPublic/pastecli/issues](issue) for inclusion.


Notes
-----

There are other utilities like [https://launchpad.net/pastebinit](pastebinit)
and [http://search.cpan.org/dist/App-Nopaste/](nopaste) which do the same job, but
they didn't work that well for me under Fedora, so I started to work on pastecli,
_stealing_ a lot of ideas from both tools, especially pastebinit.

For this inheritage a big **thank you!**, it is the strength of open source!

BTW, I need to work on the formalisms but consider this code to be GPLv3+.
